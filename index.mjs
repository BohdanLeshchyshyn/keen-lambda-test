import {S3Client, PutObjectCommand} from '@aws-sdk/client-s3';

const s3 = new S3Client({ region: 'us-east-2' });
const bucketName = 'keenethicsdevops-lambda';


export const handler = async (event) => {
  if (!event.url) {
    throw new Error('No url provided');
  }

  const url = new URL(event.url);
  const response = await global.fetch(url, { method: 'GET' });
  const buffer = Buffer.from(await response.arrayBuffer(), 'binary');
  const fileName = url.pathname.split('/').pop();
  const key = `bohdankeen/${fileName}`;

  return await s3.send(
    new PutObjectCommand({ Bucket: bucketName, Key: key, Body: buffer })
  );
};

// for testing
// await handler();
